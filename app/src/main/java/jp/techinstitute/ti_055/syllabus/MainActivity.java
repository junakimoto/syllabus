package jp.techinstitute.ti_055.syllabus;

import android.content.*;
import android.os.*;
import android.support.v7.app.*;
import android.view.*;
import android.widget.*;
import java.util.*;

public class MainActivity extends AppCompatActivity {

	private class CourseItem{
		String date;
		String title;
		String teacher;
		String detail;
	}
	
	private List<CourseItem> itemList;
	private ItemAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
/*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
*/
		itemList=new ArrayList<CourseItem>();
		adapter=new ItemAdapter(getApplicationContext(),0,itemList);
		ListView listView=(ListView)findViewById(R.id.listview);
		listView.setAdapter(adapter);
		setCourseData();
    }

	private void setCourseData() {
		CourseItem item = new CourseItem();
		item.date = "2/12";
		item.title = "test";
		item.teacher = "akimoto";
		item.detail = "detail ";
		itemList.add(item);
	}

	private class ItemAdapter extends ArrayAdapter<CourseItem> {
		private LayoutInflater inflater;

		public ItemAdapter(Context context, int resource, List<CourseItem> objects) {
			super(context, resource, objects);
			inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// TODO: Implement this method
			View view = inflater.inflate(R.layout.lecture_row, null, false);
			TextView dateView = (TextView) view.findViewById(R.id.date);
			TextView titleView = (TextView) view.findViewById(R.id.title);
			CourseItem item=getItem(position);
			dateView.setText(item.date);
			titleView.setText(item.title);
			return view;
		}
	}

	
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
